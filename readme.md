# HTML Starters

I make lots of pages, writing the boilerplate everytime is just boring

## Getting Started

Just clone the repository or download a specific one, or just copy-paste the raw code, feel free!

Some frequently used things which I will add in all templates
- Font Awesome 5
- jQuery 3.2 and above

### Prerequisites

To clone the templates, simply run the below command

```
git clone https://github.com/fayazara/htmlstarters.git
```